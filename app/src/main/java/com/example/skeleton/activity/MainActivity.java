package com.example.skeleton.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.skeleton.R;
import com.example.skeleton.adapters.SampleAdapter;
import com.example.skeleton.models.SampleModel;
import com.example.skeleton.retrofit.Api;
import com.example.skeleton.retrofit.NetworkClient;
import com.example.skeleton.utils.CommonUtils;

import static com.example.skeleton.utils.CommonUtils.COUNTRY_CODE_URL;


import org.json.JSONArray;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    private SampleAdapter imageViewAdapter;
    private List<SampleModel> imageView_models = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        recyclerView = findViewById(R.id.recyclerView);
        imageViewAdapter = new SampleAdapter(imageView_models, this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setAdapter(imageViewAdapter);
//        imageViewAdapter.notifyItemInserted(imageView_models.size() - 1);

        imageViewAdapter.notifyDataSetChanged();




        apiHit();

    }


    //@@@GET
    private void apiHit() {

        String url = COUNTRY_CODE_URL("") ;
        Log.e("url", url);

        Api apiInterface = NetworkClient.getRetrofitClient(this).create(Api.class);

        Call<ResponseBody> call = apiInterface.getApiRequest(url);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            String jsonobj = response.body().string();
                            Log.e("subdomainresponse", String.valueOf(response.body().string()));


                        } else {
                            Log.i("onEmptyResponse", "Returned empty response");
                        }
                    }
                } catch (Exception e) {


                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }


    //@@POST
    private void apiHit1() {

        String url = COUNTRY_CODE_URL("") ;
        Log.e("url", url);

        Api apiInterface = NetworkClient.getRetrofitClient(this).create(Api.class);

//        Call<ResponseBody> call = apiInterface.getApiRequest(url);
        Call<ResponseBody> callqr = apiInterface.count_of_steps(COUNTRY_CODE_URL(""),"","","","","");


        callqr.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            String jsonobj = response.body().string();
                            Log.e("subdomainresponse", String.valueOf(response.body().string()));

                            JSONObject jsonObject = new JSONObject(response.body().string());
                            Log.e("++++json+++++", String.valueOf(jsonObject));


                        } else {
                            Log.i("onEmptyResponse", "Returned empty response");
                        }
                    }
                } catch (Exception e) {


                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}




/*
    private void List_attendance(String groups_type) {
        final ProgressDialog dialog = new ProgressDialog(AttendanceDashBoard.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        String attendance_lists = attendanceListDashboard(getAccessToken(getApplicationContext())) +
                "&filter={"+"\""+"group_type"+"\":"+groups_type+","+"\""+"is_attendance"+"\""+":1}";


        Log.e("attendance_list", attendance_lists);

        Api service = ServiceGenerator.createService(Api.class, AttendanceDashBoard.this);
        Call<ResponseBody> call = service.getApiRequest(attendance_lists);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                if (dialog != null) {
                    dialog.dismiss();
                }

                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());

                    if (jsonObject.has("data")) {

                        JSONArray data = jsonObject.getJSONArray("data");
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject object = data.getJSONObject(i);


                            DashboardAttendanceModel dashboardAttendanceModel = new DashboardAttendanceModel();
                            dashboardAttendanceModel.setId(object.getString("id"));
                            dashboardAttendanceModel.setUser_id(object.getString("user_id"));
                            dashboardAttendanceModel.setGroup_name(object.getString("title"));
                            dashboardAttendanceModel.setGroup_desc(object.getString("description"));
                            dashboardAttendanceModel.setUrl(object.getString("group_image_url"));
                            dashboardAttendanceModels.add(dashboardAttendanceModel);
                        }
                        attendance_list.setAdapter(dashboardAttendanceadapter);

                        dashboardAttendanceadapter.notifyDataSetChanged();


                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();

                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(AttendanceDashBoard.this, "Time Out Error", Toast.LENGTH_SHORT).show();

                }
            }
        });


    }
*/
