package com.example.skeleton.retrofit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface Api {

    @GET()
    Call<ResponseBody> getApiRequest(@Url String apiname);

    @FormUrlEncoded
    @POST()
    Call<ResponseBody> count_of_steps(@Url String apiname,
                                      @Field("created_date") String created_date,
                                      @Field("platform") String platform,
                                      @Field("running_distance") String running_distance,
                                      @Field("running_unit") String running_unit,
                                      @Field("step_count") String step_count);
}
